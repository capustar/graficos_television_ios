//
//  DirectoTableViewController.swift
//  Television
//
//  Created by mastermoviles on 1/2/18.
//  Copyright © 2018 EPS. All rights reserved.
//

import UIKit
import AVKit

class DirectoTableViewController: UITableViewController, UAMedioTableDelegate
{
    var moviePlayer : AVPlayer?
    var controllerMoviePlayer = AVPlayerViewController()
    
    // -------------------------------------------------------------------------
    
    func saved(medio: UAMedio)
    {
        UAFuenteDatos.sharedInstance.lista_directos.append(medio)
        self.tableView.reloadData()
    }
    
    // -------------------------------------------------------------------------
    
    func anadirBotonNuevoMedio()
    {
        // Do any additional setup after loading the view, typically from a nib.
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    // -------------------------------------------------------------------------
    
    @objc func insertNewObject(_ sender: Any)
    {
        let pelisAdd = UAMedioTableViewController()
        pelisAdd.delegate = self
        
        self.navigationController?.pushViewController(pelisAdd, animated: true)
    }

    // -------------------------------------------------------------------------

    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.navigationItem.leftBarButtonItem = self.editButtonItem
        
        self.anadirBotonNuevoMedio()
        
        self.navigationItem.title = "Directo"
    }

    // -------------------------------------------------------------------------

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    // -------------------------------------------------------------------------

    override func numberOfSections(in tableView: UITableView) -> Int
    {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    // -------------------------------------------------------------------------

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of rows
        return UAFuenteDatos.sharedInstance.lista_directos.count
    }

    // -------------------------------------------------------------------------
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
     {
        let cell = tableView.dequeueReusableCell(withIdentifier: "directo_cell", for: indexPath)
        
        let directo = UAFuenteDatos.sharedInstance.lista_directos[indexPath.row]
        cell.textLabel!.text = directo.titulo
        
        cell.imageView?.contentMode = .scaleAspectFit
        return cell
    }
    
    // -------------------------------------------------------------------------
    
    @objc func reproduccionFinalizada(_ notification: Notification)
    {
        //self.moviePlayer?.view.removeFromSuperview()
        self.controllerMoviePlayer.dismiss(animated: true, completion: { () in })
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.moviePlayer?.currentItem)
        self.moviePlayer = nil
    }
    
    // -------------------------------------------------------------------------
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let directo = UAFuenteDatos.sharedInstance.lista_directos[indexPath.row]
        
        self.moviePlayer = AVPlayer(url: directo.url!)
        //self.controllerMoviePlayer.contentOverlayView?.addSubview(UIImageView(image: UIImage(named: "fondo.png")!))
        //self.controllerMoviePlayer.contentOverlayView?.backgroundColor = UIColor(red: 178/255, green: 178/255, blue: 122/255, alpha: 1)
        self.controllerMoviePlayer.player = self.moviePlayer
        self.present(self.controllerMoviePlayer, animated: true, completion: { () in })
        self.moviePlayer?.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reproduccionFinalizada), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.moviePlayer?.currentItem)
        
    }

    // -------------------------------------------------------------------------
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // -------------------------------------------------------------------------

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            // Delete the row from the data source
            UAFuenteDatos.sharedInstance.lista_directos.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        else if editingStyle == .insert
        {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

}
