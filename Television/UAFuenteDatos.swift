//
//  UAFuenteDatos.swift
//  Television
//
//  Created by mastermoviles on 1/2/18.
//  Copyright © 2018 EPS. All rights reserved.
//

import UIKit

class UAFuenteDatos
{
    //Lista de audios
    var lista_audios = [UAMedio]()
    //Lista de videos bajo demanda
    var lista_videos = [UAMedio]()
    //Lista de emisiones en directo
    var lista_directos = [UAMedio]()
    
    static let datos : UAFuenteDatos = UAFuenteDatos()
    
    // -------------------------------------------------------------------------
    
    private func rellenarDatosListaAudios()
    {
        self.lista_audios.append(UAMedio(
            titulo : "La ira de Toffana",
            artista : "La IRA",
            album : "Arte y terrorismo",
            portada : UIImage(named: "laira")!,
            url : Bundle.main.url(forResource: "La ira de Toffana", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "De pas",
            artista : "Atupa",
            album : "Quasi res porta el diario",
            portada : UIImage(named: "atupa")!,
            url : Bundle.main.url(forResource: "De pas", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Cristiano Ronaldo puto loser",
            artista : "La Vida Moderna",
            album : "",
            portada : UIImage(named: "lavidamoderna")!,
            url : Bundle.main.url(forResource: "Cristiano Ronaldo puto loser", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Editorial",
            artista : "Atupa",
            album : "Quasi res porta el diario",
            portada : UIImage(named: "atupa")!,
            url : Bundle.main.url(forResource: "Editorial", withExtension: "mp3")!))

        self.lista_audios.append(UAMedio(
            titulo : "Cancion sin nombre",
            artista : "La Otra",
            album : "Pa fuera y pa dentro",
            portada : UIImage(named: "laotra")!,
            url : Bundle.main.url(forResource: "Cancion sin nombre", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Himno URSS",
            artista : "URSS",
            album : "Comunismo",
            portada : UIImage(named: "urss")!,
            url : Bundle.main.url(forResource: "HimnoUrss", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Campiones",
            artista : "Atupa",
            album : "Quasi res porta el diario",
            portada : UIImage(named: "atupa")!,
            url : Bundle.main.url(forResource: "Campiones", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Rota la baraja",
            artista : "La IRA",
            album : "Arte y terrorismo",
            portada : UIImage(named: "laira")!,
            url : Bundle.main.url(forResource: "Rota la baraja", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Contigo",
            artista : "La Otra",
            album : "Pa fuera y pa dentro",
            portada : UIImage(named: "laotra")!,
            url : Bundle.main.url(forResource: "Contigo", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Soy",
            artista : "La IRA",
            album : "Arte y terrorismo",
            url : Bundle.main.url(forResource: "Soy", withExtension: "mp3")!))
        
        
        self.lista_audios.append(UAMedio(
            titulo : "Benvinguda al desbarat",
            artista : "Pupil-les",
            album : "Les silenciades",
            portada : UIImage(named: "pupiles")!,
            url : Bundle.main.url(forResource: "Benvinguda al Desbarat", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Orgullo machote",
            artista : "La Vida Moderna",
            album : "",
            portada : UIImage(named: "lavidamoderna")!,
            url : Bundle.main.url(forResource: "Orgullo machote", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Gresca",
            artista : "Pupil-les",
            album : "Les silenciades",
            portada : UIImage(named: "pupiles")!,
            url : Bundle.main.url(forResource: "Gresca", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Liberalo",
            artista : "La IRA",
            album : "Arte y terrorismo",
            portada : UIImage(named: "laira")!,
            url : Bundle.main.url(forResource: "Liberalo", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Coloquemonos",
            artista : "La Vida Moderna",
            album : "",
            portada : UIImage(named: "lavidamoderna")!,
            url : Bundle.main.url(forResource: "Coloquemonos", withExtension: "mp3")!))
        
        self.lista_audios.append(UAMedio(
            titulo : "Cuando no caminas conmigo",
            artista : "La Otra",
            album : "Pa fuera y pa dentro",
            url : Bundle.main.url(forResource: "Cuando no caminas conmigo", withExtension: "mp3")!))
    }
    
    // -------------------------------------------------------------------------
    
    private func rellenarDatosListaVideos()
    {
        self.lista_videos.append(UAMedio(
            titulo : "Diferente",
            artista : "Arkano",
            url : URL(string: "http://192.168.1.131:1935/vod/mp4:Diferente.mp4/playlist.m3u8")!))
        
        self.lista_videos.append(UAMedio(
            titulo : "Elvis Canario",
            artista : "La Vida Moderna",
            url : URL(string: "http://192.168.1.131:1935/vod/mp4:ElvisCanario.mp4/playlist.m3u8")!))
        
        self.lista_videos.append(UAMedio(
            titulo : "En el nombre del padre",
            artista : "Desconocido",
            url : URL(string: "http://192.168.1.131:1935/vod/mp4:EnElNombreDelPadre.mp4/playlist.m3u8")!))
        
        self.lista_videos.append(UAMedio(
            titulo : "Las gordas tambien hacemos cine",
            artista : "La Resistencia",
            url : URL(string: "http://192.168.1.131:1935/vod/mp4:LasGordasTambienHacemosCine.mp4/playlist.m3u8")!))
        
        self.lista_videos.append(UAMedio(
            titulo : "Monologo Valeria Ros",
            artista : "La Ser",
            url : URL(string: "http://192.168.1.131:1935/vod/mp4:MonologoValeriaRos.mp4/playlist.m3u8")!))
        
        self.lista_videos.append(UAMedio(
            titulo : "Nuestro planeta",
            artista : "Desconocido",
            url : URL(string: "http://192.168.1.131:1935/vod/mp4:NuestroPlaneta.mp4/playlist.m3u8")!))
        
        self.lista_videos.append(UAMedio(
            titulo : "Poco se habla de Venezuela",
            artista : "Tuerka News",
            url : URL(string: "http://192.168.1.131:1935/vod/mp4:PocoHablaVenezuela.mp4/playlist.m3u8")!))
        
        self.lista_videos.append(UAMedio(
            titulo : "Relax",
            artista : "Paulo Londra",
            url : URL(string: "http://192.168.1.131:1935/vod/mp4:Relax.mp4/playlist.m3u8")!))
        
        self.lista_videos.append(UAMedio(
            titulo : "Respuestas feministas para cunados",
            artista : "El Tornillo",
            url : URL(string: "http://192.168.1.131:1935/vod/mp4:RespuestasFeministasParaCunaos.mp4/playlist.m3u8")!))
        
        self.lista_videos.append(UAMedio(
            titulo : "Poema feminista a partir de canciones machistas",
            artista : "Alejandra Martinez",
            url : URL(string: "http://192.168.1.131:1935/vod/mp4:PoemaFeminista.mp4/playlist.m3u8")!))
        
        self.lista_videos.append(UAMedio(
            titulo : "Sample",
            artista : "Wowza",
            url : URL(string: "http://192.168.1.131:1935/vod/mp4:sample.mp4/playlist.m3u8")!))
    }
    
    // -------------------------------------------------------------------------
    
    private func rellenarDatosListaDirectos()
    {        
        self.lista_directos.append(UAMedio(
            titulo : "Canal 1",
            url : URL(string: "http://192.168.1.131:1935/live/canal1/playlist.m3u8")!))
        
        self.lista_directos.append(UAMedio(
            titulo : "Canal 2",
            url : URL(string: "http://192.168.1.131:1935/live/canal2/playlist.m3u8")!))

        self.lista_directos.append(UAMedio(
            titulo : "Canal 3",
            url : URL(string: "http://192.168.1.131:1935/live/canal3/playlist.m3u8")!))
        
        self.lista_directos.append(UAMedio(
            titulo : "Canal 4",
            url : URL(string: "http://192.168.1.131:1935/live/canal4/playlist.m3u8")!))
    }
    
    // -------------------------------------------------------------------------

    private init()
    {
        self.rellenarDatosListaAudios()
        self.rellenarDatosListaVideos()
        self.rellenarDatosListaDirectos()
    }
    
    // -------------------------------------------------------------------------

    public static var sharedInstance : UAFuenteDatos
    {
        get
        {
            return datos
        }
    }
    
    // -------------------------------------------------------------------------
}
