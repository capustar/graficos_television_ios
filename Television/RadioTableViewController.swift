//
//  RadioTableViewController.swift
//  Television
//
//  Created by mastermoviles on 1/2/18.
//  Copyright © 2018 EPS. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class RadioTableViewController: UITableViewController
{
    var player: AVAudioPlayer?
    let mpic = MPNowPlayingInfoCenter.default()
    
    // -------------------------------------------------------------------------

    @objc func routeChanged(_ notification: Notification)
    {
        guard let userInfo = notification.userInfo,
            let reasonValue = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt,
            let reason = AVAudioSessionRouteChangeReason(rawValue:reasonValue)
        else
        {
                return
        }
        
        switch reason
        {
            case .newDeviceAvailable:
                
                break
            
            case .oldDeviceUnavailable:
                
                if (self.player?.isPlaying)!
                {
                    self.player?.pause()
                }
                
                break
            
            default: ()
        }
    }
    
    // -------------------------------------------------------------------------
    
    func configurarComportamientoDesconexionAuriculares()
    {
        var nc = NotificationCenter.default
        nc.addObserver(
            self,
            selector: #selector(self.routeChanged),
            name: NSNotification.Name.AVAudioSessionRouteChange,
            object: AVAudioSession.sharedInstance())
    }
    
    // -------------------------------------------------------------------------

    func configurarAudioSession()
    {
        do
        {
            var audioSession = AVAudioSession.sharedInstance()
            
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setActive(true)
        }
        catch let error
        {
            print(error.localizedDescription)
        }
    }
    
    // -------------------------------------------------------------------------

    func recibirEventosControlRemoto()
    {
        self.becomeFirstResponder()
        UIApplication.shared.beginReceivingRemoteControlEvents()
    }

    // -------------------------------------------------------------------------

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        
        self.recibirEventosControlRemoto()
        
        self.navigationItem.title = "Radio"
        
        self.configurarAudioSession()
        
        self.configurarComportamientoDesconexionAuriculares()
    }

    // -------------------------------------------------------------------------

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // -------------------------------------------------------------------------

    override func remoteControlReceived(with event: UIEvent?)
    {
        if event?.type == .remoteControl
        {
            if event?.subtype == .remoteControlTogglePlayPause
            {
                if (self.player?.isPlaying)!
                {
                    self.player?.pause()
                }
                else
                {
                    self.player?.play()
                }
            }
            else if event?.subtype == .remoteControlPause
            {
                self.player?.pause()
            }
            else if event?.subtype == .remoteControlPlay
            {
                self.player?.play()
            }
        }
    }

    // MARK: - Table view data source

    // -------------------------------------------------------------------------

    override func numberOfSections(in tableView: UITableView) -> Int
    {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    // -------------------------------------------------------------------------

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of rows
        return UAFuenteDatos.sharedInstance.lista_audios.count
    }

    // -------------------------------------------------------------------------

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "radio_cell", for: indexPath)
        
        let audio = UAFuenteDatos.sharedInstance.lista_audios[indexPath.row]
        cell.textLabel!.text = audio.titulo
        cell.detailTextLabel?.text = audio.artista
        
        cell.imageView?.contentMode = .scaleAspectFit
        if audio.portada != nil
        {
            cell.imageView?.image = audio.portada
        }
        else
        {
            cell.imageView?.image =  UIImage(named: "default")
        }
        
        return cell
    }
    
    // -------------------------------------------------------------------------

    func mostrarMetadatosAudio(_ titulo : String, _ artista : String, _ album : String, _ portada : UIImage?)
    {
        let mySize = CGSize(width: 400, height: 400)
        let albumArt = MPMediaItemArtwork(boundsSize:mySize)
        { sz in
            if portada != nil
            {
                return portada!
            }
            else
            {
                return UIImage(named: "default")!
            }
        }
        
        let info = [MPMediaItemPropertyTitle: titulo,
                    MPMediaItemPropertyArtist: artista,
                    MPMediaItemPropertyAlbumTitle: album,
                    MPMediaItemPropertyArtwork : albumArt] as [String : Any]
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = info
    }
    
    // -------------------------------------------------------------------------

    func reproducirSonido(_ url_sonido : URL)
    {
        do
        {
            player = try AVAudioPlayer(contentsOf: url_sonido)
            player?.prepareToPlay()
            player?.play()
        }
        catch let error
        {
            print(error.localizedDescription)
        }
    }
    
    // -------------------------------------------------------------------------

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let audio = UAFuenteDatos.sharedInstance.lista_audios[indexPath.row]

        self.reproducirSonido(audio.url!)
        
        self.mostrarMetadatosAudio(audio.titulo!, audio.artista!, audio.album!, audio.portada)
    }
    
    // -------------------------------------------------------------------------

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // -------------------------------------------------------------------------

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            // Delete the row from the data source
            UAFuenteDatos.sharedInstance.lista_audios.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        else if editingStyle == .insert
        {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
