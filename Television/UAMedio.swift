//
//  UAMedio.swift
//  Television
//
//  Created by mastermoviles on 1/2/18.
//  Copyright © 2018 EPS. All rights reserved.
//

import UIKit

class UAMedio
{
    var titulo : String?
    var artista : String?
    var album : String?
    var portada : UIImage?
    var url : URL?
    
    // -------------------------------------------------------------------------
    
    init(titulo: String, artista: String, album: String, portada : UIImage, url : URL)
    {
        self.titulo = titulo
        self.artista = artista
        self.album  = album
        self.portada = portada
        self.url = url
    }
    
    // -------------------------------------------------------------------------
    
    init(titulo: String, artista: String, album: String, url : URL)
    {
        self.titulo = titulo
        self.artista = artista
        self.album  = album
        self.url = url
    }
    
    // -------------------------------------------------------------------------
    
    init(titulo: String, artista: String, album: String)
    {
        self.titulo = titulo
        self.artista = artista
        self.album  = album
    }
    
    // -------------------------------------------------------------------------
    
    init(titulo: String, artista: String, url : URL)
    {
        self.titulo = titulo
        self.artista = artista
        self.url = url
    }
    
    // -------------------------------------------------------------------------
    
    init(titulo: String, url : URL)
    {
        self.titulo = titulo
        self.url = url
    }
    
    // -------------------------------------------------------------------------
    
    init()
    {
        self.titulo = ""
        self.artista  = ""
        self.album = ""
    }
}
