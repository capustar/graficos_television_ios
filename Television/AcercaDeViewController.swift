//
//  FirstViewController.swift
//  Television
//
//  Created by mastermoviles on 1/2/18.
//  Copyright © 2018 EPS. All rights reserved.
//

import UIKit

class AcercaDeViewController: UIViewController
{
    
    // -------------------------------------------------------------------------

    func generarGrafica()
    {
        // Creamos vista y añadimos a  la vista actual
        let pieChartView = PieChartView()
        pieChartView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        let cantidad_audios = UAFuenteDatos.sharedInstance.lista_audios.count
        let cantidad_videos = UAFuenteDatos.sharedInstance.lista_videos.count
        let cantidad_directos = UAFuenteDatos.sharedInstance.lista_directos.count
        
        pieChartView.segments =
        [
            Segment(color: UIColor.red, value: CGFloat(cantidad_directos), texto: "Vídeo en directo"),
            Segment(color: UIColor.blue, value: CGFloat(cantidad_videos), texto: "Vídeo bajo demanda" ),
            Segment(color: UIColor.yellow, value: CGFloat(cantidad_audios), texto: "Audio")
        ]
        pieChartView.titulo = "PorPlusClubVideo\nDesarrollado por Marcelo Morán"
        view.addSubview(pieChartView)
    }
    
    // -------------------------------------------------------------------------

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.generarGrafica()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(deviceRotated),
            name: NSNotification.Name.UIDeviceOrientationDidChange,
            object: nil)
    }

    // -------------------------------------------------------------------------

    override func viewWillDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }

    // -------------------------------------------------------------------------

    @objc func deviceRotated()
    {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation)
        {
            self.generarGrafica()
        }
        else if UIDeviceOrientationIsPortrait(UIDevice.current.orientation)
        {
            self.generarGrafica()
        }
    }

    // -------------------------------------------------------------------------

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.generarGrafica()
    }

    // -------------------------------------------------------------------------

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

