//
//  UAMedioTableViewController.swift
//  Television
//
//  Created by MarceloMoran on 8/2/18.
//  Copyright © 2018 EPS. All rights reserved.
//

import UIKit

protocol UAMedioTableDelegate
{
    func saved(medio : UAMedio)
}

class UAMedioTableViewController: UITableViewController, UITextFieldDelegate, UITextViewDelegate
{
    var medio = UAMedio()
    var delegate : UAMedioTableDelegate?

    // -------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // This will remove extra separators from tableview
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        var titulo_boton : String;
        var titulo_barra : String;
        
        titulo_boton = "Guardar"
        titulo_barra = "Nuevo UAMedio"
        
        let saveButton = UIBarButtonItem(title: titulo_boton, style: .done, target: self, action: #selector(save))
        self.navigationItem.rightBarButtonItem = saveButton
        self.navigationItem.title = titulo_barra
        
    }

    // -------------------------------------------------------------------------
    
    func getCellTextField(section : Int) -> String?
    {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: section))
        let textField = cell?.subviews[1] as? UITextField
        return textField?.text
    }
    
    // -------------------------------------------------------------------------
    
    func getCellTextView(section : Int) -> String?
    {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: section))
        let textView = cell?.subviews[1] as? UITextView
        return textView?.text
    }
    
    
    // -------------------------------------------------------------------------
    
    @objc func save()
    {
        if let titulo = self.getCellTextField(section: 0)
        {
            self.medio.titulo = titulo
        }
        if let artista = self.getCellTextField(section: 1)
        {
            self.medio.artista = artista
        }
        if let url = self.getCellTextField(section: 2)
        {
            self.medio.url = URL(string: url)
            print(self.medio.url?.absoluteString)
        }
        
        self.delegate?.saved(medio: self.medio)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // -------------------------------------------------------------------------
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    // -------------------------------------------------------------------------
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    // -------------------------------------------------------------------------
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch(section) {
        case 0: return "Titulo"
        case 1: return "Artista"
        case 2: return "URL"
        default: return nil
        }
    }
    
    // -------------------------------------------------------------------------
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        cell.accessoryType = .none
        
        let textField = UITextField(frame: CGRect(x:10,y:10,width:self.tableView.frame.size.width-10, height:30))
        
        textField.adjustsFontSizeToFitWidth = true
        textField.delegate = self
        textField.tag = indexPath.section
        textField.isEnabled = true
        
        if indexPath.section == 0
        {
            textField.text = self.medio.titulo
        }
        else if indexPath.section == 1
        {
            textField.text = self.medio.artista
        }
        else
        {
            textField.text = self.medio.url?.absoluteString
        }
        
        cell.addSubview(textField)
        
        return cell
    }
    
    // -------------------------------------------------------------------------
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 40
    }
    
    // -------------------------------------------------------------------------
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    // -------------------------------------------------------------------------
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    // -------------------------------------------------------------------------
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        switch textField.tag
        {
        case 0: self.medio.titulo = textField.text
        case 1: self.medio.artista = textField.text
        case 2: self.medio.url = URL(string: textField.text!)
        default: print("Unknown!") //  Esto no debería pasar nunca, añadido para silenciar el warning
        }
    }
    
    // -------------------------------------------------------------------------
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        
    }
    
    // -------------------------------------------------------------------------
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return false
    }
    
    // -------------------------------------------------------------------------
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

}
